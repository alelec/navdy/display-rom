DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"
pushd superrs-kitchen > /dev/null

if [[ ! -d superr_navdy ]]; then
ln -s ../ ./superr_navdy
fi

export project=navdy
export romname=navdy

romdir=$DIR
sysdir=$DIR/system
prfiles=$DIR/00_project_files
framedir=$DIR/system/framework
appdir=$DIR/system/app
privdir=$DIR/system/priv-app
usdir=$DIR/META-INF/com/google/android
logs=$DIR/00_project_files/logs

androidversion=$(grep ro.build.version.release $DIR/system/build.prop | cut -d= -f2)
api=$(grep ro.build.version.sdk $DIR/system/build.prop |  cut -d= -f2)

devicename=navdy_hud_6dl
devicechk=ro.product.device

deviceloc=$DIR/superrs-kitchen/tools/devices/navdy_hud_6dl

export 'usesudo=sudo '

source $DIR/superrs-kitchen/tools/language/english-srk

odexstatus=Deodexed
permtype=set_metadata

whatimg=system
imgfile=sys

imgtest=$(du -hbd 0 $DIR/system | gawk '{ print $1 }')
# tmpsize=$(cat "$deviceloc/${imgfile}imgsize")
tmpsize=$(($imgtest + 60000000))
syssize=$tmpsize

sparseimg=' -s'

# make_ext4fs=$DIR/superrs-kitchen/tools/linux_tools/make_ext4fs_64
# simg2img=$DIR/superrs-kitchen/tools/linux_tools/simg2img_64
# vdexext=$DIR/superrs-kitchen/tools/linux_tools/vdexExtractor_64
# lz4=$DIR/superrs-kitchen/tools/boot/AIK/bin/linux/x86_64/lz4

# cd $DIR

#partimg() {
#	banner
	echo "$bluet$t_img_create_symlinks $whatimg.img ...$normal"
	pushd $romdir
	line=""
	cat $prfiles/symlinks | while read line; do
		target=$(echo "$line" | cut -d"\"" -f2)
		link=$(echo "$line" | cut -d"\"" -f4 | sed 's/^\///')
		linkdir=$(dirname $link)
		if [[ ! -d $linkdir ]]; then
			mkdir -p $linkdir
		fi
		ln -s -f -T $target $link
	done
	# banner
	fcontexts=""
	if [[ -f $prfiles/file_contexts ]]; then
		fcontexts=" -S $prfiles/file_contexts"
	fi
	echo "$bluet$t_img_create_raw $whatimg.img ...$normal"
	if [[ $whatimg = "system" ]]; then
		if [[ -f $romdir/system/init.rc && -d $romdir/system/system/app ]]; then
			make_ext4fs -T 0$fcontexts -l $syssize -L / -a /$sparseimg ${whatimg}_new.img ${whatimg}/
		else
			make_ext4fs -T 0$fcontexts -l $syssize -a $whatimg$sparseimg ${whatimg}_new.img ${whatimg}/
		fi
	elif [[ $whatimg = "data" ]]; then
		( make_ext4fs -T 0$fcontexts -l $datsize -a $whatimg$sparseimg ${whatimg}_new.img ${whatimg}/ 2>&1 ) 
	elif [[ -s $prfiles/exdirsb && $(grep "$whatimg" $prfiles/exdirsb) ]]; then
		partsize=$(grep "$whatimg" $prfiles/exdirsb | gawk '{print $2}')
		( make_ext4fs -T 0$fcontexts -l $partsize -a $whatimg$sparseimg ${whatimg}_new.img ${whatimg}/ 2>&1 )
	fi
	sparseimg=""
	if [[ -d vendor ]]; then
		find system vendor -type l -exec rm -f {} \;
	else
		find system -type l -exec rm -f {} \;
	fi
	if [[ ! -f ${whatimg}_new.img ]]; then
		# banner
		echo "$redb$yellowt$bold$t_error$normal"
		echo -e "$t_img_fail"
		echo ""
		# read -p "$t_enter_main_menu"
		# cd $base
		popd > /dev/null
	fi
#}
