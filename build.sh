#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" &> /dev/null

#apt install -y dos2unix sudo gawk android-tools-fsutils zip coreutils cpio

VNUM=$(git describe --tags | cut -d'-' -f1)

# If not on tag, increment previous one
ON_TAG=$(git describe --exact-match --tags HEAD 2> /dev/null)
test $ON_TAG || VNUM=$((VNUM + 1))
VERS=${VNUM}-$(git rev-parse --short HEAD)

echo -e "\n"\
"***********************\n"\
" Building ${VERS}      \n"\
"***********************\n"

set -e # Any commands which fail will cause the shell script to exit

bash ./bootimg/unpackimg.sh boot.img

bash ./set_version.sh $VNUM

echo '
****************
 Build boot.img
****************'
bash ./bootimg/repackimg.sh
mv ./bootimg/image-new.img ./boot.img
sudo chown $(whoami) boot.img

sudo mv ./bootimg/ramdisk/init.freescale.debug.rc ./bootimg/ramdisk/init.freescale.rc

bash ./bootimg/repackimg.sh
mv ./bootimg/image-new.img ./boot-debug.img
sudo chown $(whoami) boot-debug.img

echo '
*******************
 Build recovery.img
*******************'
pushd recoveryimg/ramdisk
sudo mkdir -p tmp dev sys data proc
sudo chmod 777 tmp dev sys data proc
cd ..
sudo ./repackimg.sh
sudo mv image-new.img ../recovery.img
chown $(whoami) ../recovery.img
popd

echo '
***************
 Build Hud.apk
***************'
pushd ./Hud
rm -rf ./build/outputs/apk
sh ./gradlew assemble
popd

echo '
***************
 Build Obd.apk
***************'
pushd ./Obd
rm -rf ./build/outputs/apk
sh ./gradlew assemble
popd


build_system_dist () {
  echo '
  ******************
   Build system.img
  ******************'
  ./build_system_img.sh

  OUT=$DIR/dist/navdy-display-alelec-${VERS}$1
  rm -rf ${OUT} 2> /dev/null
  rm -rf ${OUT}.zip 2> /dev/null
  mkdir -p ${OUT}


  echo '
  ***************
   Assemble Dist
  ***************'
  cp -al tools/* $OUT/
  chmod -R 777 $OUT/*
  cp -l boot$2.img $OUT/boot.img
  mv system_new.img $OUT/system.img
  cp -l recovery.img $OUT/
  cp -l android-info.txt $OUT/

  pushd $OUT &> /dev/null
  for IMG in system.img boot.img recovery.img; do
  echo "$IMG $(stat -c %s $IMG) $(sha256sum $IMG | cut -d' ' -f1)" >> info.txt
  done
  echo "version: ${VNUM}" >> info.txt
  popd &> /dev/null

  echo $OUT
  ls -l $OUT
  pushd $OUT
  zip -r $OUT.zip *
  popd
  echo "
  Built ${OUT}
  "
}


BRANCH=$(test $ON_TAG && echo $ON_TAG || echo "master")

echo '
******************
  RELEASE BUILD
******************'
perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=user\n#' system/build.prop
perl -0777 -i -pe 's#:userdebug/release-keys#:user/release-keys#' system/build.prop

rm -rf ./system/priv-app/Hud/Hud*apk
rm -rf ./system/priv-app/Hud/lib/arm/*
rm -rf ./system/priv-app/Obd/Obd*apk

touch ./system/priv-app/Hud/new_install

cp ./Hud/build/outputs/apk/legacy/release/Hud-*-release*.apk ./system/priv-app/Hud/Hud.legacy.apk.rom

cp ./Hud/build/outputs/apk/current/release/Hud-*-release*.apk ./system/priv-app/Hud/Hud.apk
cp ./Hud/build/intermediates/transforms/mergeJniLibs/current/release/0/lib/armeabi-v7a/* ./system/priv-app/Hud/lib/arm/

cp ./Obd/build/outputs/apk/release/Obd-*-release.apk ./system/priv-app/Obd/Obd.apk.rom
cp ./Obd/build/outputs/apk/release/Obd-*-release.apk ./system/priv-app/Obd/Obd.apk

chmod -R 755 ./system/priv-app/Hud
chmod -R 755 ./system/priv-app/Obd

cp system/etc/bluetooth/bt_stack_release.conf system/etc/bluetooth/bt_stack.conf

build_system_dist

echo '
******************
    DEBUG BUILD
******************'
perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=eng\n#' system/build.prop
perl -0777 -i -pe 's#:user/release-keys#:eng/release-keys#' system/build.prop

rm -rf ./system/priv-app/Hud/Hud*apk
rm -rf ./system/priv-app/Hud/lib/arm/*
rm -rf ./system/priv-app/Obd/Obd*apk

touch ./system/priv-app/Hud/new_install

cp ./Hud/build/outputs/apk/legacy/debug/Hud-*-debug*.apk ./system/priv-app/Hud/Hud.legacy.apk.rom

cp ./Hud/build/outputs/apk/current/debug/Hud-*-debug*.apk ./system/priv-app/Hud/Hud.apk
cp ./Hud/build/intermediates/transforms/mergeJniLibs/current/debug/0/lib/armeabi-v7a/* ./system/priv-app/Hud/lib/arm/

cp ./Obd/build/outputs/apk/debug/Obd-*-debug.apk ./system/priv-app/Obd/Obd.apk.rom
cp ./Obd/build/outputs/apk/debug/Obd-*-debug.apk ./system/priv-app/Obd/Obd.apk

chmod -R 755 ./system/priv-app/Hud
chmod -R 755 ./system/priv-app/Obd

cp system/etc/bluetooth/bt_stack_debug.conf system/etc/bluetooth/bt_stack.conf

build_system_dist -debug

echo '
DONE'
popd &> /dev/null
