#!/usr/bin/env python3
import os
import time
import shutil
import argparse
import subprocess
from pathlib import Path


parser = argparse.ArgumentParser()
parser.add_argument("--proj", default="Hud", help="Set submodule folder name")
parser.add_argument("--branch", default="master", help="Submodule branch artifact to download")
parser.add_argument("--job", default="release", help="Submodule build job artifact to download")
args = parser.parse_args()


target = Path(__file__).parent / str(args.proj)

url = subprocess.run(f'git config -f .gitmodules --get submodule.{args.proj}.url', shell=True, stdout=subprocess.PIPE).stdout.decode().strip()
if url.startswith("git@"):
    url = "https://" + url[4:].replace(':', "/")
if url.endswith(".git"):
    url = url[:-4]

url += f"/-/jobs/artifacts/{args.branch}/download?job={args.job}"

try:
    token = os.environ['PRIVATE_TOKEN']
    url = URLS[-1] + f"&private_token={token}"
except:
    token = None

print(f"Downloading {args.proj} from:", url)

subprocess.run(f'curl -L -k -v -o {target.name}.zip "{url}" 2> /dev/null', shell=True, cwd=str(target.parent))

if os.system(f'grep "<!DOCTYPE html>" {target}.zip >/dev/null') == 0:
    if target.exists():
        broken_target = target.with_suffix(".broken")
        target.rename(broken_target)
    else:
        broken_target = "repo"
    print(f"ERROR: binary not downloaded correctly, check {broken_target}")
    exit(-1)

subprocess.run(f'unzip ../{target.name}.zip',
               shell=True, cwd=str(target))

print(target)
