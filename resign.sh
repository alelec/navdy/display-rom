SYS_CERT_APKS="\
system/framework/framework-res.apk
system/priv-app/Shell/Shell.apk
system/priv-app/DefaultContainerService/DefaultContainerService.apk
system/priv-app/ExternalStorageProvider/ExternalStorageProvider.apk
system/priv-app/Hud/Hud.apk
system/priv-app/FusedLocation/FusedLocation.apk
system/priv-app/SettingsProvider/SettingsProvider.apk
system/priv-app/Settings/Settings.apk
system/priv-app/Obd/Obd.apk
system/priv-app/MiniLauncher/MiniLauncher.apk
system/priv-app/ANCS/ANCS.apk
system/priv-app/OneTimeInitializer/OneTimeInitializer.apk
system/priv-app/InputDevices/InputDevices.apk
system/app/KeyChain/KeyChain.apk
system/app/CertInstaller/CertInstaller.apk
system/app/GalleryLite/GalleryLite.apk
system/app/PicoTts/PicoTts.apk
system/app/Bluetooth/Bluetooth.apk
system/app/Provision/Provision.apk
system/app/PackageInstaller/PackageInstaller.apk
"

for APK in $SYS_CERT_APKS
do
apksigner sign --ks navdy_alelec.jks --ks-pass=pass:alelec --out $APK $APK
done


