mount /dev/block/mmcblk2p4   /maps 2> /dev/null
mkdir -p /maps/update

shopt -s nullglob
for update in /maps/update/*.zip
  do
    date
    mkfifo /tmp/progress
    fbsplash -s /res/recovery_logo.ppm -d /dev/graphics/fb0 -i /res/splash.ini  -f /tmp/progress &

    echo "Installing: $update"
    echo -e "10" > /tmp/progress

    cd /maps/update
    rm -rf unpack
    mkdir unpack
    cd unpack

    date
    # TODO 45 seconds
    PROGRESS="10"
    unzip -o "$update" & pid=$!
    while kill -0 $pid 2> /dev/null; do
      if [ "$PROGRESS" -lt "35" ]; then
        PROGRESS=$(($PROGRESS + 1))
        echo -e "$PROGRESS"
        echo -e "$PROGRESS" > /tmp/progress
      fi
      sleep 2;
    done

    mv "${update}" "${update}.done"
    date

    echo -e "35" > /tmp/progress

    echo "Flashing: boot.img"
    dd if=boot.img bs=64k of=/dev/block/mmcblk2p1
    echo -e "37" > /tmp/progress

    echo "Flashing: recovery.img"
    dd if=recovery.img bs=64k of=/dev/block/mmcblk2p2
    echo -e "40" > /tmp/progress

    date
    echo "Converting sparse:  system.img"
    PROGRESS="40"
    simg2img system.img system.raw.img & pid=$!

    while kill -0 $pid 2> /dev/null; do
      if [ "$PROGRESS" -lt "50" ]; then
        PROGRESS=$(($PROGRESS + 1))
        echo -e "$PROGRESS"
        echo -e "$PROGRESS" > /tmp/progress
      fi
      sleep 2;
    done

    date
    echo "Flashing: system.raw.img"

    #dd if=system.raw.img bs=1M count=110 of=/dev/block/mmcblk2p5
    #echo -e "60" > /tmp/progress

    #dd if=system.raw.img bs=1M count=110 skip=110 of=/dev/block/mmcblk2p5
    #echo -e "70" > /tmp/progress

    #dd if=system.raw.img bs=1M count=110 skip=220 of=/dev/block/mmcblk2p5
    #echo -e "80" > /tmp/progress

    #dd if=system.raw.img bs=1M count=110 skip=330 of=/dev/block/mmcblk2p5
    #echo -e "90" > /tmp/progress

    #dd if=system.raw.img bs=1M skip=440 of=/dev/block/mmcblk2p5

    PROGRESS="50"
    dd if=system.raw.img bs=1M of=/dev/block/mmcblk2p5 & pid=$!

    while kill -0 $pid 2> /dev/null; do
      if [ "$PROGRESS" -lt "95" ]; then
        PROGRESS=$(($PROGRESS + 1))
        echo -e "$PROGRESS"
        echo -e "$PROGRESS" > /tmp/progress
      fi
      sleep 1;
    done

    echo -e "100" > /tmp/progress

    date
    echo "Finished"
    echo -e "exit" > /tmp/progress
    sleep 3
    /sbin/recovery --just_exit
    break
done
